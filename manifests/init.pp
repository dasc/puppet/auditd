class auditd (
    String $service_ensure,
    String $service_provider,
    Boolean $service_enable,
    String $audit_rules,
    Boolean $use_syslog,
    String $audisp_dir,
    String $audisp_plugin_dir,
    String $log_file,
    Integer $log_file_sizemb,
    Integer $log_file_num,
    Boolean $listen = false,
    Optional[Integer] $port = undef,
    Optional[String] $remote_host = undef,
) {
    package {'audit': ensure => 'installed', }
    package {'audispd-plugins': ensure => 'installed', }
    service {'auditd':
        ensure   => $service_ensure,
        enable   => $service_enable,
        provider => $service_provider,
    }

    file { '/etc/audit/rules.d/audit.rules':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0600',
        source => $audit_rules,
        notify => Service['auditd'],
    }

    file { '/etc/audit/auditd.conf':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0640',
        content => epp('auditd/auditd.conf.epp', {
            port            => $port,
            listen          => $listen,
            log_file        => $log_file,
            log_file_sizemb => $log_file_sizemb,
            log_file_num    => $log_file_num,
        }),
        notify  => Service['auditd'],
    }

    file { "${audisp_plugin_dir}/syslog.conf":
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0640',
        content => epp('auditd/audisp-syslog.conf.epp', {
            use_syslog => $use_syslog,
        }),
        notify  => Service['auditd'],
        require => Package['audit'],
    }
    file {'/etc/systemd/system/auditd.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/auditd/etc/systemd/system/auditd.service',
        notify  => [Service['auditd'], Exec['auditd daemon-reload']],
        require => Package['audit', 'audispd-plugins'],
    }
    if $remote_host and $remote_host != '' {
        file {"${audisp_dir}/audisp-remote.conf":
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0640',
            content => epp('auditd/audisp-remote.conf.epp', {
                port        => $port,
                remote_host => $remote_host,
            }),
            notify  => [Service['auditd'], Exec['auditd daemon-reload']],
            require => Package['audit', 'audispd-plugins'],
        }
        file {"${audisp_plugin_dir}/au-remote.conf":
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0640',
            content => epp('auditd/au-remote.conf.epp', {
                enable => true,
            }),
            notify  => [Service['auditd'], Exec['auditd daemon-reload']],
            require => Package['audit', 'audispd-plugins'],
        }
    } else {
        file {"${audisp_dir}/audisp-remote.conf":
            ensure  => 'absent',
            notify  => [Service['auditd'], Exec['auditd daemon-reload']],
            require => Package['audit', 'audispd-plugins'],
        }
        file {"${audisp_plugin_dir}/au-remote.conf":
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0640',
            content => epp('auditd/au-remote.conf.epp', {
                enable => false,
            }),
            notify  => [Service['auditd'], Exec['auditd daemon-reload']],
            require => Package['audit', 'audispd-plugins'],
        }
    }
    exec {'auditd daemon-reload':
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>
}
